package com.example.vpos.emvdemo;

import android.device.PiccManager;
import android.os.Message;
import android.util.Log;

public class Picc {
    private volatile static Picc singleton;
    PiccManager picc;
    private Picc() {
        picc = new PiccManager();
    }

    public static Picc getInstance() {
        if (singleton == null) {
            synchronized (Icc.class) {
                if (singleton == null) {
                    singleton = new Picc();
                }
            }
        }
        return singleton;
    }
    
    public int open() {
        return picc.open();
    }
    
    public int close() {
        return picc.close();
    }
    
    public int request(int mode, byte[] cardType, byte[] serialNo) {
        byte Atq[] = new byte[14];
        char SAK = 1;
        byte sak[] = new byte[1];
        sak[0] = (byte) SAK;
        byte SN[] = new byte[10];
        int scan_card = picc.request(cardType, Atq);
        int SNLen = -1;
        if(scan_card > 0) {
            SNLen = picc.antisel(serialNo, sak);
            if(SNLen != -1)
            return 0;
        }
        return -1;
    }
    
    public int activate() {
        return picc.activate();
    }
    
    public int apduTransmit(byte[] apdu, int apduLen, byte[] rsp, byte[] rspStatus) {
        return picc.apduTransmit(apdu, apduLen, rsp, rspStatus);
    }
}
