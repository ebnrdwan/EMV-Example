package com.example.vpos.emvdemo;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.device.MaxqManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.vpos.vpos.emvkernel.EMVCAPK;
import com.example.vpos.vpos.emvkernel.EMV_APPLIST;
import com.example.vpos.vpos.emvkernel.EMV_PARAM;
import com.example.vpos.vpos.emvkernel.EmvKernel;

import java.io.File;

//import com.qsposcom.SerialPortNative;

public class MainActivity extends Activity {

	TextView textViewMsg = null;
	private Button btnIcc;
	private Button btnPicc;
	public String tag = "EmvActivity";
	//PiccManager Picc = null;
	//IccManager Icc = null;
	int iret;

	int glOnlineTrans;
	int gReversal;
	Test_Thread Test_Thread = null;
//	private static final String FILEPATH="/extsd/solomon.com.testcinvakejava/";
	private static final String FILEPATH="/data/data/com.example.vpos.emvdemo/";
	//private static final String FILEPATH="/data/wwan/solomon.com.testcinvakejava/";
	//private static final String FILEPATH="/mnt/sdcard/data/solomon.com.testcinvakejava/";
	//private static final String FILEPATH="/mnt/sdcard/solomon.com.testcinvakejava/";
	private File destDir;

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emv);
//		SerialPortNative.initHardWare();
		textViewMsg = (TextView) this.findViewById(R.id.textView_msg);
		btnIcc = (Button) findViewById(R.id.button1);
		btnIcc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(Test_Thread != null && !Test_Thread.m_bThreadFinished){
                    return;
                }
                Test_Thread = new Test_Thread(0);
                Test_Thread.start();
            }
        });
		btnPicc = (Button) findViewById(R.id.button2);
		btnPicc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(Test_Thread != null && !Test_Thread.m_bThreadFinished){
                    return;
                }
                Test_Thread = new Test_Thread(1);
                Test_Thread.start();
            }
        });
		destDir = new File(this.FILEPATH);
		if (!destDir.exists()) {
			if (destDir.mkdirs()) {
				Log.i("make dir", "make dir OK");
				destDir.setExecutable(true);
				destDir.setReadable(true);
				destDir.setWritable(true);
			} else {
				Log.i("make dir", "make dir failed");
			}
		} else {
			Log.i("make dir", "make dir, dir exist");
			destDir.setExecutable(true);
			destDir.setReadable(true);
			destDir.setWritable(true);

			for (File f : destDir.listFiles()) {
				Log.i("delete file" , f.toString());
				f.delete();
			}
		}
		EmvKernel.EmvLib_SetContext(this);
		//Picc = new PiccManager();
		//Icc = new IccManager();
		maxq = new MaxqManager();
	}
	MaxqManager maxq;
	@Override
	protected void onStart() {
		super.onStart();
		maxq.open();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Icc.getInstance().close();
		Picc.getInstance().close();
		maxq.close();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
//		SerialPortNative.HardWarePowerOff();
		System.exit(0);
		Log.e("onDestroy", "onDestroy HardWarePowerOff");
	}

	public void SendMsg(String strInfo, int what){

		Message msg = new Message();
		msg.what = what;
		Bundle b = new Bundle();
		b.putString("MSG", strInfo);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	public class Test_Thread extends Thread {

		int testMode, ret;
        long TransAmount = 500;
        long TransNo = 0;
		boolean m_bThreadFinished = true;
		EMV_PARAM TermParam;
		byte PART_MATCH = 0;

		byte[] AppName = new byte[33];
		byte[] AID = {(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x33};
		byte AidLen = 5;
		byte SelFlag = PART_MATCH;
		byte Priority = 0;
		byte TargetPer = 0;
		byte MaxTargetPer = 0;
		byte FloorLimitCheck = 1;
		byte RandTransSel = 1;
		byte VelocityCheck = 1;
		long FloorLimit = 2000;
		long Threshold = 0;
		byte[] TACDenial = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00};
		byte[] TACOnline = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00};
		byte[] TACDefault = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00};
		byte[] AcquierId = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x12,(byte)0x34,(byte)0x56};
		byte[] dDOL = {(byte)0x03,(byte)0x9F,(byte)0x37,(byte)0x04};
		byte[] tDOL = {(byte)0x0F,(byte)0x9F,(byte)0x02,(byte)0x06,(byte)0x5F,(byte)0x2A,(byte)0x02,(byte)0x9A,(byte)0x03,(byte)0x9C,(byte)0x01,(byte)0x95,(byte)0x05,(byte)0x9F,(byte)0x37,(byte)0x04};
		byte[] Version = {(byte)0x00,(byte)0x30};
		byte[] RiskManData = new byte[10];
		byte EC_bTermLimitCheck = 0;
		long EC_TermLimit = 10000;

		byte CL_bStatusCheck = 1;
		long CL_FloorLimit = 10000;
		long CL_TransLimit = 50000;
		long CL_CVMLimit = 20000;
		byte TermQuali_byte2 = 0;
		int slot=0;
		public Test_Thread(int slot) {
		this.slot = slot;
			TermParam = new EMV_PARAM();
		}

		public void run() {
			Log.e("ScanThread[ run ]", "run() begin");
			m_bThreadFinished = false;
			SendMsg("", 2);

			synchronized (this) {
				do{
					SendMsg("init...", 0);

					EmvKernel.EmvLib_SetFilePath(FILEPATH); //

					ret = EmvKernel.EmvLib_Init();
					if(ret != 0){
						SendMsg("EmvLib_Init Fail ret = " + ret, 0);
						break;
					}

					SendMsg("Add Capk and aid...", 0);
					{
						byte[] RID = {(byte) 0xA0,0x00,0x00,0x00,0x03};
						byte KeyID = (byte) 0x90;
						byte HashInd = 0x01;
						byte ArithInd = 0x01;
						byte ModulLen = 64;
						byte[] Modul = {(byte)0xC2,(byte)0x6B,(byte)0x3C,(byte)0xB3,(byte)0x83,(byte)0x3E,(byte)0x42,(byte)0xD8,(byte)0x27,(byte)0x0D,(byte)0xC1,(byte)0x0C,(byte)0x89,(byte)0x99,(byte)0xB2,(byte)0xDA,
								(byte)0x18,(byte)0x10,(byte)0x68,(byte)0x38,(byte)0x65,(byte)0x0D,(byte)0xA0,(byte)0xDB,(byte)0xF1,(byte)0x54,(byte)0xEF,(byte)0xD5,(byte)0x11,(byte)0x00,(byte)0xAD,(byte)0x14,
								(byte)0x47,(byte)0x41,(byte)0xB2,(byte)0xA8,(byte)0x7D,(byte)0x68,(byte)0x81,(byte)0xF8,(byte)0x63,(byte)0x0E,(byte)0x33,(byte)0x48,(byte)0xDE,(byte)0xA3,(byte)0xF7,(byte)0x80,
								(byte)0x38,(byte)0xE9,(byte)0xB2,(byte)0x1A,(byte)0x69,(byte)0x7E,(byte)0xB2,(byte)0xA6,(byte)0x71,(byte)0x6D,(byte)0x32,(byte)0xCB,(byte)0xF2,(byte)0x60,(byte)0x86,(byte)0xF1};

						byte ExponentLen = 1;
						byte[] Exponent = {0x03};
						byte[] ExpDate = {0x15,0x12,0x31};
						byte[] CheckSum = {(byte) 0xB3,(byte)0xAE,0x2B,(byte)0xC3,(byte)0xCA,(byte)0xFC,0x05,(byte)0xEE,(byte)0xEF,(byte)0xAA,0x46,(byte)0xA2,(byte)0xA4,0x7E,(byte)0xD5,0x1D,
								(byte)0xE6,0x79,(byte)0xF8,0x23};

						EMVCAPK capk_visa_t90 = new EMVCAPK(RID, KeyID, HashInd, ArithInd, ModulLen, Modul, ExponentLen, Exponent,
								ExpDate, CheckSum);
						ret = EmvKernel.EmvLib_AddCapk(capk_visa_t90);
						if(ret != 0){
							SendMsg("EmvLib_AddCapk Fail ret = " + ret, 0);
							break;
						}
					}

					//CUP_TEST_APP
					{

						byte[] AID1 = {(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x33,(byte)0x01,(byte)0x01,(byte)0x02};
						byte AidLen1 = 8;

						EMV_APPLIST CUP_TEST_APP = new EMV_APPLIST(AppName, AID1, AidLen1, SelFlag,
								Priority, TargetPer, MaxTargetPer,
								FloorLimitCheck, RandTransSel, VelocityCheck,
								FloorLimit, Threshold, TACDenial,
								TACOnline, TACDefault, AcquierId, dDOL,
								tDOL, Version, RiskManData, EC_bTermLimitCheck,
								EC_TermLimit, CL_bStatusCheck,CL_FloorLimit, CL_TransLimit,
								CL_CVMLimit, TermQuali_byte2);
						ret = EmvKernel.EmvLib_AddApp(CUP_TEST_APP);
						if(ret != 0){
							SendMsg("EmvLib_AddApp Fail", 0);
							break;
						}
					}
					//PBOC_TEST_APP
					{
						byte[] AID2 = {(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x33,(byte)0x01,(byte)0x1};
						byte AidLen2 = 7;

						EMV_APPLIST PBOC_TEST_APP = new EMV_APPLIST(AppName, AID2, AidLen2, SelFlag,
								Priority, TargetPer, MaxTargetPer,
								FloorLimitCheck, RandTransSel, VelocityCheck,
								FloorLimit, Threshold, TACDenial,
								TACOnline, TACDefault, AcquierId, dDOL,
								tDOL, Version, RiskManData, EC_bTermLimitCheck,
								EC_TermLimit,CL_bStatusCheck, CL_FloorLimit, CL_TransLimit,
								CL_CVMLimit, TermQuali_byte2);
						ret = EmvKernel.EmvLib_AddApp(PBOC_TEST_APP);	//D1 3D
						if(ret != 0){
							SendMsg("EmvLib_AddApp Fail", 0);
							break;
						}
					}
	                   //PBOC_TEST_APP
                    {
                        byte[] AID3 = {(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x04,(byte)0x10,(byte)0x10};//masterCard
                        byte AidLen3 = 7;

                        EMV_APPLIST PBOC_TEST_APP = new EMV_APPLIST(AppName, AID3, AidLen3, SelFlag,
                                Priority, TargetPer, MaxTargetPer,
                                FloorLimitCheck, RandTransSel, VelocityCheck,
                                FloorLimit, Threshold, TACDenial,
                                TACOnline, TACDefault, AcquierId, dDOL,
                                tDOL, Version, RiskManData, EC_bTermLimitCheck,
                                EC_TermLimit,CL_bStatusCheck, CL_FloorLimit, CL_TransLimit,
                                CL_CVMLimit, TermQuali_byte2);
                        ret = EmvKernel.EmvLib_AddApp(PBOC_TEST_APP);   //D1 3D
                        if(ret != 0){
                            SendMsg("EmvLib_AddApp Fail", 0);
                            break;
                        }
                    }

                  //PBOC_TEST_APP
                    {
                        byte[] AID4 = {(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x10,(byte)0x10};//VISA 外卡
                        byte AidLen4 = 7;

                        EMV_APPLIST PBOC_TEST_APP = new EMV_APPLIST(AppName, AID4, AidLen4, SelFlag,
                                Priority, TargetPer, MaxTargetPer,
                                FloorLimitCheck, RandTransSel, VelocityCheck,
                                FloorLimit, Threshold, TACDenial,
                                TACOnline, TACDefault, AcquierId, dDOL,
                                tDOL, Version, RiskManData, EC_bTermLimitCheck,
                                EC_TermLimit,CL_bStatusCheck, CL_FloorLimit, CL_TransLimit,
                                CL_CVMLimit, TermQuali_byte2);
                        ret = EmvKernel.EmvLib_AddApp(PBOC_TEST_APP);   //D1 3D
                        if(ret != 0){
                            SendMsg("EmvLib_AddApp Fail", 0);
                            break;
                        }
                    }
					ret = EmvKernel.EmvLib_GetParam(TermParam);	//��ȡEMV����
					if(ret != 0){
						SendMsg("EmvLib_GetParam Fail ret = " + ret, 0);
						break;
					}

					{
						//����������޸�EMV����
						ret = EmvKernel.EmvLib_SetParam(TermParam);	//��ȡEMV����
						if(ret != 0){
							SendMsg("EmvLib_SetParam Fail ret = " + ret, 0);
							break;
						}
					}

					if(slot == 0)   //IC��
					{
						while(true){
							iret = DetectInput();//�쿨
							if(iret != 0)
								continue;

							ProcessIcc(TransNo,TransAmount);//���?Ƭ����
							TransNo += 1;

							break;
						}
					}
					else if(slot == 1)  //�ǽӿ�
					{
						ret = EmvKernel.EmvLib_qPBOCPreProcess(TransAmount);//qpbocԤ����
						if(ret != 0){
							SendMsg("EmvLib_qPBOCPreProcess ret = " + ret, 0);
							break;
						}
						else
						{
							while(true){
								iret = DetectInputCL();//�쿨
								if(iret != 0)
									continue;

								ProcessPicc(TransNo,TransAmount);//���?Ƭ����
								TransNo += 1;

								break;
							}
						}
					}


				}while(false);
				m_bThreadFinished = true;
				return;
			}
	    }
	};
int DetectInput(){

		int ret = -1;
		byte slot = 0;
		byte vcc_mode = 1;
		byte ATR[] = new byte[40];
		ret = Icc.getInstance().open();//Icc.open((byte)0, (byte)0x01, (byte)0x01);//Icc.Lib_IccOpen(slot, vcc_mode, ATR);
		//ret = 0;
		SendMsg("Icc.Lib_IccOpen ret = " + ret, 0);
		if(ret != 0){
			Log.e(tag, "Lib_IccOpen failed!");
			//Icc.Lib_IccClose(slot);
			Icc.getInstance().close();
			return -1;
		}
		else
		{
		    int status = Icc.getInstance().detect();//Icc.detect();
		    //ret = 0;
		    SendMsg("Icc.detect ret = " + status, 0);
            if (status != 0) {
                Log.e(tag, "Lib_IccOpen detect failed!");
                status = Icc.getInstance().close();
            }

            byte[] atr = new byte[64];
            //status = Icc.getInstance().activate(atr);
            Log.e(tag, "Lib_IccOpen activate failed!" + status);
            status = Icc.getInstance().close();
			return 0;
		}
	}
	int DetectInputCL(){

		int ret;
		byte mode = 0;
		byte cardType[] = new byte[10];
		byte serialNo[] = new byte[50];

		SendMsg("swipe Card, Please", 0);
		ret = Picc.getInstance().open();//Picc.Lib_PiccOpen();
		SendMsg("Picc.Lib_PiccOpen ret = " + ret, 0);
		SleepMs(1300);
		if(ret == 0)
		{

			//ret = Picc.request(arg0, arg1)//Picc.Lib_PiccCheck(mode, cardType, serialNo);//��⵽�ǽӿ�Ƭ
		    ret = Picc.getInstance().request(mode, cardType, serialNo);
			SendMsg("Picc.Lib_PiccCheck ret = " + ret, 0);
			SleepMs(1300);
			if(ret == 0)
			{
				return 0;
			}
			return -1;
		} else {

			return -1;
		}
	}
	void ProcessIcc(long transno,long TransAmount) //����qpboc������
	{
		int ret;
		byte ifonline[] ={0,0};
		long backamt = 0;
		byte tdate[] = {(byte) 0x15,(byte)0x05,(byte)0x15};//���ڣ���ʽΪ����ֽ� ��15 05 15���20150515
		byte ttime[] = {(byte) 0x09,(byte)0x20,(byte)0x00};//ʱ�䣬��ʽΪ����ֽ��� 09 20 00���9:20:00

		byte Result;
		byte RspCode[] = {0x30,0x30};
		byte AuthCode[] = new byte[6];
		int AuthCodeLen = 0;
		byte IAuthData[] = new byte[64];
		int IAuthDataLen = 0;
		byte Script[]= new byte[128];
		int ScriptLen = 0;

		//ʵ���У����ں�ʱ��Ӧ���õ�ǰ�����ں�ʱ�䣬����д����

	    int len[] = new int[1];
	    byte buff[] = new byte[80];

		Log.e("", "ProcessIcc Begin................");
//	    while (true)
	    {
	    	ret = EmvKernel.EmvLib_BeforeTrans(TransAmount,backamt,tdate,ttime);//�����׽����ֽ����ڣ�ʱ�����ú�
			if(ret != 0){
				SendMsg("EmvLib_BeforeTrans Fail, ret = " + ret, 0);
				return;
			}

			Log.e("", "EmvLib_AppSel Begin................");
	    	ret = EmvKernel.EmvLib_AppSel(0, transno);	//��һ�������Ƿǽ�qpboc�Ŀ��ۣ��ڶ��������ǽ�����ˮ��
	    	//Ӧ��ѡ�����GPO
			if(ret != 0){
				SendMsg("EmvLib_AppSel Fail, ret = " + ret, 0);
				return;
			}

			Log.e("", "EmvLib_ReadAppData Begin................");
			ret = EmvKernel.EmvLib_ReadAppData();// ����¼
			if(ret != 0){
				SendMsg("EmvLib_ReadAppData Fail, ret = " + ret, 0);
				return;
			}

			Log.e("", "EmvLib_CardAuth Begin................");
			ret = EmvKernel.EmvLib_CardAuth();// �ѻ������֤
			if(ret != 0){
				SendMsg("EmvLib_CardAuth Fail, ret = " + ret, 0);
				return;
			}

			Log.e("", "EmvLib_ProcRestriction Begin................");
			EmvKernel.EmvLib_ProcRestriction();// ��������

			Log.e("", "EmvLib_CardholderVerify Begin................");
			ret = EmvKernel.EmvLib_CardholderVerify();//�ֿ�����֤

			if(ret != 0){
				SendMsg("EmvLib_CardholderVerify Fail ret = " + ret, 0);
				return;
			}
			Log.e("", "EmvLib_ProcTransBeforeOnline Begin................");
	        ret = EmvKernel.EmvLib_ProcTransBeforeOnline(0,ifonline);//���״���
	        if(ret != 0){
				SendMsg("EmvLib_ProcTransBeforeOnline Fail ret = " + ret, 0);
				return;
			}
	        Log.e("", "EmvLib_GetTLV Begin................");
			ret=EmvKernel.EmvLib_GetTLV("9a", buff,  len);	//�����ͨ������ӿڻ�ȡ����Ҫ����tagֵ
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.d("", "EmvLib_GetTLV 9a ret = " + ret + " len =  " + len[0]);
			Log.d("", "EmvLib_GetTLV 9a buff = " + Convert.bytesToHexString(buff, 0, len[0]));

			ret=EmvKernel.EmvLib_GetTLV("95", buff,  len);	//D1 17
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.d("", "EmvLib_GetTLV 95 ret = " + ret + " len =  " + len[0]);
			Log.d("", "EmvLib_GetTLV 95 buff = " + Convert.bytesToHexString(buff, 0, len[0]));
			// card pan
			ret=EmvKernel.EmvLib_GetTLV("5a", buff,  len);	//card pan
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.d("", "EmvLib_GetTLV 5a ret = " + ret + " len =  " + len[0]);
			Log.d("", "EmvLib_GetTLV 5a buff = " + Convert.bytesToHexString(buff, 0, len[0]));
			SendMsg("Test Finish", 0);

			ret=EmvKernel.EmvLib_GetTLV("57", buff,  len);	//card 2 track data as msr
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.d("", "EmvLib_GetTLV 57 ret = " + ret + " len =  " + len[0]);
			Log.d("", "EmvLib_GetTLV 57 buff = " + Convert.bytesToHexString(buff, 0, len[0]));
			SendMsg("Test Finish", 0);
			if(ifonline[0] == 1)
			{
				{
					Result = 0;//������ 0���ɹ�
					RspCode[0] = 0x30;
					RspCode[1] = 0x30;  //��Ȩ��Ӧ��

				}
				Log.d("", "EmvLib_ProcTransComplete");
				ret=EmvKernel.EmvLib_ProcTransComplete(Result,RspCode,AuthCode,AuthCodeLen,IAuthData,IAuthDataLen,Script,ScriptLen);	//�����ͨ������ӿڻ�ȡ����Ҫ����tagֵ
				if(ret != 0){
					SendMsg("EmvLib_ProcTransComplete Fail ret = " + ret, 0);
					return;
				}
			}
	    }
	}
	void ProcessPicc(long transno,long TransAmount) //����qpboc������
	{
		int ret;

		long backamt = 0;
		byte ifonline[] = {0,0};
		byte tdate[] = {(byte) 0x15,(byte)0x05,(byte)0x15};//���ڣ���ʽΪ����ֽ� ��15 05 15���20150515
		byte ttime[] = {(byte) 0x09,(byte)0x20,(byte)0x00};//ʱ�䣬��ʽΪ����ֽ��� 09 20 00���9:20:00

		//ʵ���У����ں�ʱ��Ӧ���õ�ǰ�����ں�ʱ�䣬����д����

	    int len[] = new int[1];
	    byte buff[] = new byte[80];

	    byte Result;
		byte RspCode[] = {0,0};
		byte AuthCode[] = new byte[6];
		int AuthCodeLen = 0;
		byte IAuthData[] = new byte[64];
		int IAuthDataLen = 0;
		byte Script[]= new byte[128];
		int ScriptLen = 0;

		Log.e("", "ProcessPicc Begin................");
//	    while (true)
	    {
	    	ret = EmvKernel.EmvLib_BeforeTrans(TransAmount,0,tdate, ttime);	//�����׽����ֽ����ڣ�ʱ�����ú�
	    	//Ӧ��ѡ�����GPO
			if(ret != 0){
				SendMsg("EmvLib_BeforeTrans Fail, ret = " + ret, 0);
				return;
			}
	    	ret = EmvKernel.EmvLib_AppSel(1, transno);	//��һ�������Ƿǽ�qpboc�Ŀ��ۣ��ڶ��������ǽ�����ˮ��
	    	//Ӧ��ѡ�����GPO
			if(ret != 0){
				SendMsg("EmvLib_AppSel Fail, ret = " + ret, 0);
				return;
			}

			ret = EmvKernel.EmvLib_ReadAppData();// ����¼
			if(ret != 0){
				SendMsg("EmvLib_ReadAppData Fail, ret = " + ret, 0);
				return;
			}

			ret = EmvKernel.EmvLib_CardAuth();// �ѻ������֤
			if(ret != 0){
				SendMsg("EmvLib_CardAuth Fail, ret = " + ret, 0);
				return;
			}


	        ret = EmvKernel.EmvLib_ProcCLTransBeforeOnline(1,ifonline);//���״���
	        if(ret != 0){
				SendMsg("EmvLib_ProcCLTransBeforeOnline Fail ret = " + ret, 0);
				return;
			}

			ret=EmvKernel.EmvLib_GetTLV("9a", buff,  len);	//�����ͨ������ӿڻ�ȡ����Ҫ����tagֵ
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.e("111", "EmvLib_GetTLV ret = " + ret + " len =  " + len[0]);
			Log.e("111", "EmvLib_GetTLV buff = " + Convert.bytesToHexString(buff, 0, len[0]));

			ret=EmvKernel.EmvLib_GetTLV("95", buff,  len);	//D1 17
			if(ret != 0){
				SendMsg("EmvLib_GetTLV Fail ret = " + ret, 0);
				return;
			}
			Log.e("111", "EmvLib_GetTLV ret = " + ret + " len =  " + len[0]);
			Log.e("111", "EmvLib_GetTLV buff = " + Convert.bytesToHexString(buff, 0, len[0]));
			SendMsg("Test Finish", 0);
			if(ifonline[0] == 1)
			{
				{

					//�˴�������������55�򣬻�ȡ��̨���ص����
					Result = 0;//������ 0���ɹ�
					RspCode[0] = 0x30;
					RspCode[1] = 0x30;  //��Ȩ��Ӧ��

				}

				ret=EmvKernel.EmvLib_ProcCLTransComplete(Result,RspCode,AuthCode,AuthCodeLen);	//D1 17
				if(ret != 0){
					SendMsg("EmvLib_ProcCLTransComplete Fail ret = " + ret, 0);
					return;
				}
			}

	    }
	}

	void SleepMs(int ms){
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			String strInfo = b.getString("MSG");
			if(msg.what == 0)
				textViewMsg.setText(strInfo);
			else if(msg.what == 1){
				textViewMsg.setText(textViewMsg.getText().toString() + "\n" + strInfo);
			}else {
//				Toast.makeText(getApplicationContext(), strInfo, Toast.LENGTH_LONG).show();
				textViewMsg.setText("");
			}
			Log.d(tag, strInfo);
		}
	};
}
