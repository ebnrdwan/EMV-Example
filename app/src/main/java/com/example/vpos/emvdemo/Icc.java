
package com.example.vpos.emvdemo;

import android.device.IccManager;

public class Icc {
    private volatile static Icc singleton;
    IccManager icc;
    private Icc() {
        icc = new IccManager();
    }

    public static Icc getInstance() {
        if (singleton == null) {
            synchronized (Icc.class) {
                if (singleton == null) {
                    singleton = new Icc();
                }
            }
        }
        return singleton;
    }
    
    public int open() {
        return icc.open((byte)0, (byte)0x01, (byte)0x01);
    }
    
    public int close() {
        return icc.close();
    }
    
    public int detect() {
        return icc.detect();
    }
    
    public int activate(byte[] atr) {
        return icc.activate(atr);
    }
    
    public int apduTransmit(byte[] apdu, int apduLen, byte[] rsp) {
        byte[] rspStatus = new byte[2];
        return icc.apduTransmit(apdu, apduLen, rsp, rspStatus);
    }
}
